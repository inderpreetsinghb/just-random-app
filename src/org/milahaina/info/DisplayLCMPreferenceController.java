package org.milahaina.info;

import android.content.Context;

import android.app.Dialog;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import org.milahaina.info.R;
import androidx.annotation.IdRes;
import androidx.preference.Preference;

public class DisplayLCMPreferenceController extends Preference {

  private View mView;
  private SparseArray<View> mViews = new SparseArray<>();

  @SuppressWarnings("unchecked")
  public <T extends View> T findViewById(@IdRes int id) {
    View view = mViews.get(id);
    if (view == null) {
      view = mView.findViewById(id);
      mViews.put(id, view);
    }
    return (T) view;
  }

    private String displaysysfsnode = FileUtils.readLine("/sys/class/mi_display/disp-DSI-0/panel_info");

    public DisplayLCMPreferenceController(Context context/*, String key*/) {
        super(context/*, key*/);
        TextView textView = (TextView) findViewById(R.id.lcm_summary);
        textView.setText(displaysysfsnode);
    }

}
