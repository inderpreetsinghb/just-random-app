package org.milahaina.info;

import android.os.Bundle;

import com.android.settingslib.collapsingtoolbar.CollapsingToolbarBaseActivity;
import com.android.settingslib.widget.R;

public class milahainainfoActivity extends CollapsingToolbarBaseActivity {

    private static final String TAG_MILAHAINA_INFO = "milahina-dev-check";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(R.id.content_frame,
                new milahainainfoFragment(), TAG_MILAHAINA_INFO).commit();
    }
}
